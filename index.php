<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Генератор записей</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/styles.min.css?h=1743540720844842f6d2af45ade3dc8c">
    <link rel="stylesheet" href="/bootstap/album.css">
</head>

<body><div><noscript>&lt;div&gt;&lt;img src="https://mc.yandex.ru/watch/39705265" style="  position: absolute;
  left: -9999px;
" alt="Yandex.Metrika" /&gt;&lt;/div&gt;</noscript>
        <header>
            <div class="collapse bg-dark" id="navbarHeader">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-md-7 py-4">
                            <h4 class="text-white">Мероприятия</h4>
                            <p class="text-muted">Вся информация о мероприятиях университета. Вы можете выбрать мероприятие и подать заявку на участие.</p>
                        </div>
                        <div class="col-sm-4 offset-md-1 py-4">
                            <h4 class="text-white">Информация </h4>
                            <ul class="list-unstyled">
                                <li><a href="https://mospolytech.ru/index.php?id=3885#" class="text-white">Расписание</a></li>
                                <li><a href="http://stud.mami.ru/#" class="text-white">Личный кабинет</a></li>
                                <li><a href="https://mospolytech.ru/index.php?id=3579#" class="text-white">ЦРС</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="navbar navbar-dark bg-dark shadow-sm">
                <div class="container d-flex justify-content-between"><a href="http://qsadad.std-828.ist.mospolytech.ru/#" class="navbar-brand d-flex align-items-center"><img src="/img/mos.png" width="180" height="50" alt=""> <svg width="20" height="20" class="mr-2"></svg></a><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader"><span class="navbar-toggler-icon text-center" style="font-size: 10pt;">=</span></button></div>
            </div>
        </header>
        <main>
            <section class="jumbotron text-center">
                <div class="container">
                    <h1 class="jumbotron-heading">Информация о Московском политехническом университете </h1>
                    <p class="lead text-muted">Для проведения занятий ВУЗ располагает постоянно обновляемой технической базой: в 21 корпусе размещены аудитории, учебные и научные лаборатории, компьютерные классы, спортивные залы, спортивно-оздоровительные базы, научно-техническая
                        библиотека с фондом около 2 млн экземпляров, столовые и буфеты. Университет является крупнейшим высшим учебным заведением, готовящим квалифицированных специалистов.</p>
                    <p><a href="https://mospolytech.ru/index.php#" class="btn btn-secondary my-2">Главный сайт</a>&nbsp;&nbsp;<a href="https://mospolytech.ru/index.php?id=3547#" class="btn btn-secondary my-2">Мероприятия</a></p>
                </div>
            </section>
            <div class="modal fade" id="exampleModal" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4>Запись на мероприятие</h4><button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group"><label for="recipient-name" class="col-form-label">Имя:</label><input type="text" class="form-control" id="name"></div>
                                <div class="form-group"><label for="recipient-name" class="col-form-label">Номер телефона:</label><input class="form-control" id="tel"></div>
                                <div class="form-group"><label for="recipient-name" class="col-form-label">Почта:</label><input class="form-control" id="email"></div>
                            </form>
                        </div>
                        <div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Назад</button><button id="send" class="btn btn-secondary" data-dismiss="modal">Записаться</button></div>
                    </div>
                </div>
            </div>
<?php include_once('content.php');?>
        </main>
        <footer class="text-muted">
            <div class="container">
                <p class="float-right"><a href="#"  >В начало </a></p>
                <p >Сайт разработан для Московского политехнического университета</p>
                <p >Что нового? Посетите <a  href="https://mospolytech.ru/index.php"  >главную страницу </a> чтобы узнать все новости <a href="https://mospolytech.ru/index.php?id=13" >о Московском политехническом университете</a>.</p>
            </div>
        </footer>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/script.min.js?h=b98bc821fb603a332ffb9a40e901a57d"></script>

</body>

</html>
