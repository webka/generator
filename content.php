<?php
include('db.php');
$mysqli = new mysqli($dbHost, $dbUser, $dbPass, $dbName);
$result=mysqli_query($mysqli, "SELECT * FROM `content` ORDER BY `EFDATE`");
//$lst = array();
$r=1;
//var_dump(mysqli_connect_errno());
?>	

<div class="album py-5 bg-light">
<div class="container">
<?php
while($row = mysqli_fetch_array($result)){
?>
<?php if($r%3==1){?><div class="row"><?php ;}?>
<div class="col-md-4">
    <div class="card mb-4 shadow-sm"><img class="img-fluid" src="<?=$row['LINK']?>">
        <div class="card-body">
            <p class="card-text"><?=$row['ENAME']?></p>
            <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                    <div><a class="btn btn-sm btn-outline-secondary" role="button" data-toggle="modal" href="#modal-info-<?=$row['EID']?>">Подробнее</a>
                        <div role="dialog" tabindex="-1" class="modal fade" id="modal-info-<?=$row['EID']?>">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4><?=$row['ENAME']?></h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                                    <div class="modal-body">
                                        <p class="text-left text-muted">
                                            <strong>Дата и время проведения: </strong><br /><?=$row['EDATE']?>&nbsp;&nbsp;<?=$row['ETIME']?><br />
                                            <br /><strong>Место проведения: </strong><br /><?=$row['EPLACE']?><br />
                                            <br /><strong>Описание: </strong><br /><?=$row['EDES']?><br />
                                            <br /><a href="<?=$row['EWEB']?>" >Перейти на сайт</a>
                                        </p>
                                    </div>
                                    <div class="modal-footer"><button class="btn btn-secondary" data-dismiss="modal" type="button">Закрыть</button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <button type="button" id="<?=$row['EID']?>" class="btn btn-sm btn-outline-secondary participate" data-toggle="modal" data-target="#exampleModal">Записаться</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if($r%3==0){?></div><?php ;}?>

<?php
  $r=$r+1;
}
?>
</div>
</div>
